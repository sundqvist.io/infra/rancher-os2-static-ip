#!/bin/bash

set -e

CONFIG=${1:-config.yaml}
LATEST=$(curl -s 'https://quay.io/api/v1/repository/costoolkit/os2/tag/?limit=1' -H 'accept: application/json' | jq -r '.tags[0].name')
echo $LATEST; echo
[[ -f ros-image-build ]] || curl -o ros-image-build "https://raw.githubusercontent.com/rancher-sandbox/os2/master/ros-image-build" #${LATEST%-amd64}

envsubst < $CONFIG > cloud-init.yaml

export DOCKER_BUILDKIT=1 # to make build/output.iso actually appear
export BUILDKIT_PROGRESS=plain
bash ros-image-build "quay.io/costoolkit/os2:$LATEST" iso cloud-init.yaml

mv build/output.iso "rancher-os_$LATEST.iso"
